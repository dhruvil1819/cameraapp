//stack navigator for the main screen

import * as React from 'react';
import { View, Text } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

//Screens
import Home from '../screens/home';
import Photo from '../screens/photo';

function HomeStack() {
    return (
        <Stack.Navigator screenOptions={{
          headerStyle: {
            backgroundColor: '#f4511e',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}>
          <Stack.Screen name="Home" component={Home} />
          <Stack.Screen name="Photo" component={Photo} />
        </Stack.Navigator>
    );
  }
  
  export default HomeStack;
