import React from 'react';
import { SafeAreaView, Image, StyleSheet } from 'react-native';
import _ from 'lodash';

class Photo extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {uri} = _.get(this.props, 'route.params');
        console.log(this.props.route.params);
        return (
            <SafeAreaView style={styles.container}>
               <Image source={{uri}} style={styles.photo}> 
               </Image>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    photo: {
      flex: 1,
    }
})

export default Photo;
