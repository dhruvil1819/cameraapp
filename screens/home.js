import React from 'react';
import {SafeAreaView, Text, StyleSheet, TouchableOpacity} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import Toast from 'react-native-simple-toast';
import _ from 'lodash';


const ImageOptions = {
    mediaType: 'photo',
    maxWidth: 700,
    maxHeight: 700,
    quality: 1,
}
class Home extends React.Component {
    constructor(props) {
        super(props);
    }

    navigateToPhoto(uri) {
        const { navigation } = this.props;
        navigation.navigate('Photo', {uri});
    }

    openCamera() {
        ImagePicker.launchCamera(ImageOptions, (response) => {
            if(response.error) {
                Toast.showWithGravity(response.error, Toast.LONG, Toast.BOTTOM);
            } else if(response.didCancel) {
                Toast.showWithGravity('Please take photo', Toast.LONG, Toast.BOTTOM);
            } else {
               const uri = _.get(response, 'uri', '');
               this.navigateToPhoto(uri);
            }
          });
    }

    render() {
        return(
            <SafeAreaView style={styles.container}>
               <TouchableOpacity style={styles.button} onPress={() => this.openCamera()}>
                 <Text style={styles.buttonText}>{'Take Photo'}</Text>
               </TouchableOpacity>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
   container: {
       flex: 1,
       alignItems: 'center',
       justifyContent: 'center',
   } ,
   button: {
       backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 20,
        paddingVertical: 5,
   },
   buttonText: {
       color: 'white',
       fontWeight: 'bold',
       fontSize: 16,
   }
})

export default Home;